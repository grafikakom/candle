# Projekt: Świeca

Wymodelowany płomieś świecy za pomocą cząstek.

# Struktura projektu

.   
├── images   
│   ├── checkerboard.jpg   
│   ├── icon-info.png   
│   ├── raindrop2flip.png   
│   ├── smokeparticle.png   
│   ├── snowflake.png   
│   ├── spark.png   
│   ├── spikey.png   
│   └── star.png   
├── Particle_Engine_2_files   
│   ├── base.css   
│   ├── DAT.GUI.min.js   
│   ├── Detector.js   
│   ├── icon-info.png   
│   ├── images   
│   │   ├── ui-bg_flat_75_ffffff_40x100.png   
│   │   ├── ui-bg_highlight-soft_75_cccccc_1x100.png   
│   │   └── ui-icons_222222_256x240.png   
│   ├── info.css   
│   ├── info.js   
│   ├── jquery-1.9.1.js   
│   ├── jquery-ui.css   
│   ├── jquery-ui.js   
│   ├── OrbitControls.js   
│   ├── ParticleEngineExamples.js   
│   ├── ParticleEngine.js   
│   ├── Stats.js   
│   ├── Three.js   
│   ├── THREEx.FullScreen.js   
│   ├── THREEx.KeyboardState.js   
│   └── THREEx.WindowResize.js   
├── Particle_Engine_2.html   
└── README.md   

# Inspiracje

Inspiracją do naszego projektu był Pan Lee Stekomski, który wprowadził nas w świat ThreeJsa
[stemkoski](https://stemkoski.github.io/Three.js/Particle-Engine.html)

# Jak skorzystać?

Uruchamianie:
Dokumentacja Three.js [link](https://threejs.org/docs/index.html#manual/introduction/How-to-run-thing-locally)
Korzystalismy z _Node.js server_

sterowanie świeczką:
za pomocą myszki zmienia się położenie kamery, a strzałkami w górę i w dół zmienia się wielkość płomienia
